#include "compiler.hpp"

using namespace Tungsten;

uint8_t Tungsten::get_data_size(const CompileData& data, const AST* ast)
{
	switch(ast->get_type()) { // TODO Member load
		case EXPRESSION_S: {
			const auto& children = ast->get_children();
			if(children.empty()) return 0;

			for(auto i = children.crbegin();
				i != children.crend()
				&& i->get()->get_type() == OPERATOR_S; ++i) {
				const auto size = get_data_size(data, i->get());
				if(size != 0) return size;
			}

			return get_data_size(data, children.front().get());
		}

		case VALUE_S: {
			return ((const ASTValue*) ast)->get_size();
		}

		case STRING_S: return 8;

		case OPERATOR_S: {
			const auto op = ((const ASTOperator*) ast)->get_operator();
			return (op >= EQUALS_O && op <= HIGHER_OR_EQUALS_O ? 1 : 0);
		}

		case ADDRESS_OF_S: return 8;

		case VAR_LOAD_S: {
			const auto name = ((const ASTVarLoad*) ast)->get_variable();
			return data.get_variable(name).type.get_size();
		}

		case VAR_INCREMENT_S: {
			// TODO
			break;
		}

		case VAR_DECREMENT_S: {
			// TODO
			break;
		}

		case FUNCTION_CALL_S: {
			const auto name = ((const ASTFunctionCall*) ast)->get_function();
			return data.get_function(name).return_type.get_size();
		}

		case ARRAY_LOAD_S: {
			// TODO
			break;
		}

		case SIZEOF_S: return 4;
		default: return 0;
	}

	return 0;
}

static uint8_t get_cast_step_size(const uint8_t total_size)
{
	uint8_t i = 8;

	while(i >= 1) {
		if(total_size >= i) break;
		i /= 2;
	}

	return (i >= 1 ? i : 0);
}

void Tungsten::cast(CompileData& data, const uint8_t from, const uint8_t to)
{
	if(from == to) return;
	if(from == 0 || to == 0) return;

	uint8_t i = from;

	if(from < to) {
		while(i < to) {
			const auto s = get_cast_step_size(to - i);
			data.assembly.add_inst("push"s + to_string(s) + " 0", true);

			i += s;
		}
	} else {
		while(i > to) {
			const auto s = get_cast_step_size(i - to);
			data.assembly.add_inst("pop"s + to_string(s), true);

			i -= s;
		}
	}
}

void Tungsten::cancel_stack_value(CompileData& data, const AST* ast)
{
	const auto size = get_data_size(data, ast);
	if(size == 0) return;

	data.assembly.add_inst("pop"s + std::to_string(size), true);
}
