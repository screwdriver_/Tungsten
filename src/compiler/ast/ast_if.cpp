#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTIf::compile(CompileData& data) const
{
	if(children.empty() || children.size() > 2) ERR_INTERNAL();
	if(children.front()->get_type() != EXPRESSION_S) ERR_INTERNAL();

	children.front()->compile(data);

	const auto size = get_data_size(data, children.front().get());
	if(size != 1) cast(data, size, 1);

	CompileData d(data.fork());

	for(const auto& s : scope) {
		s->compile(d);
		cancel_stack_value(data, s.get());
	}

	const auto jump = data.assembly.insts + d.assembly.insts + 2;
	data.assembly.add_inst("inv1", true);
	data.assembly.add_inst("ifjmp "s + std::to_string(jump), true);
	data.merge(d);

	if(children.size() == 2) children[1]->compile(data);
}

string ASTIf::to_string() const
{
	return "If block";
}
