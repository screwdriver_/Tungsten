#include "ast.hpp"

using namespace Tungsten;

void ASTVarIncrement::compile(CompileData& data) const
{
	const auto var = data.get_variable(variable);
	const auto size = std::to_string(var.type.get_size());
	const auto position = std::to_string(data.get_variable_position(variable));

	data.assembly.add_inst("vload"s + size + ' ' + position, true);
	data.assembly.add_inst("incr"s + size, true);
	data.assembly.add_inst("vstore"s + size + ' ' + position, true);
}

string ASTVarIncrement::to_string() const
{
	return "Variable `"s + variable + "` increment";
}
