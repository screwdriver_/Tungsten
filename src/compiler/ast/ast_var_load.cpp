#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTVarLoad::compile(CompileData& data) const
{
	const auto var = data.get_variable(variable);
	data.assembly.add_inst("vload"s + std::to_string(var.type.get_size())
		+ ' ' + std::to_string(data.get_variable_position(variable)), true);
}

string ASTVarLoad::to_string() const
{
	return "Variable load `"s + variable + '`';
}
