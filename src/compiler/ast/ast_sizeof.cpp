#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTSizeof::compile(CompileData& data) const
{
	if(children.size() != 1) ERR_INTERNAL();
	if(children.front()->get_type() != EXPRESSION_S) ERR_INTERNAL();

	data.assembly.add_inst("push4 "s
		+ std::to_string(get_data_size(data,
		(const ASTExpression*) children.front().get())), true);
}

string ASTSizeof::to_string() const
{
	return "Sizeof statement";
}
