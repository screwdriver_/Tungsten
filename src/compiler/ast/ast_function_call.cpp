#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTFunctionCall::compile(CompileData& data) const
{
	const auto func = data.get_function(function);

	for(const auto& c : children) c->compile(data);
	data.assembly.add_inst("call "s + func.get_asm_name(), true);
}

string ASTFunctionCall::to_string() const
{
	return "Function `"s + function + "` call";
}
