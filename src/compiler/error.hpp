#ifndef ERROR_HPP
# define ERROR_HPP

# include "compiler.hpp"
# include "parser/parser.hpp"

# define ERR_CANNOT_READ_FILE(file)	throw parse_exception("Cannot read file `"s + file + "`!")
# define ERR_INVALID_PREPROCESSOR()	throw parse_exception("Invalid preprocessor instruction!")
# define ERR_MACRO_NOT_FOUND(macro)	throw parse_exception("Macro `"s + macro + "` not found!")

# define ERR_EXPECTED(c)					throw parse_exception("Expected `"s + c + "`!")
# define ERR_UNEXPECTED(c)					throw parse_exception("Unexpected `"s + c + "`!")
# define ERR_UNKNOWN_KEYWORD(word)			throw parse_exception("Unknown keyword `"s + word + "`!")
# define ERR_UNEXPECTED_KEYWORD(keyword)	throw parse_exception("Unexpected keyword `"s + get_keyword(keyword) + "`!")
# define ERR_UNKNOWN_OPERATOR()				throw parse_exception("Unknown operator!")
# define ERR_UNEXPECTED_OPERATOR(op)		throw parse_exception("Unexpected operator `"s + get_operator(op) + "`!")
# define ERR_INVALID_OPERAND()				throw parse_exception("Invalid operand!")
# define ERR_GLOBAL_VARIABLE()				throw parse_exception("Forbidden declaration of variable outside of function or structure!")
# define ERR_CONSTANT_NOT_INIT(var)			throw parse_exception("Constant `"s + var + "` not initialized!")
# define ERR_TYPE(type)						throw parse_exception("Not a primitive type: `"s + get_keyword(type) + "`!")
# define ERR_NAME_EXPECTED()				throw parse_exception("Expected name for declaration!")
# define ERR_SWITCH_VALUE()					throw parse_exception("Invalid switch case value!")
# define ERR_SYNTAX()						throw parse_exception("Syntax error!")
# define ERR_UEOF()							throw parse_exception("Unexpected end-of-file!")

# define ERR_INTERNAL()				throw compile_exception("Internal error (this is probably a bug, you should report it)")
# define ERR_VAR_NOT_FOUND(var)		throw compile_exception("Variable `"s + var + "` not found!")
# define ERR_FUNC_NOT_FOUND(func)	throw compile_exception("Function `"s + func + "` not found!")
# define ERR_MUST_RETURN(func)		throw compile_exception("Reaching end of non-void function `"s + func + "` without returning a value!")
# define ERR_NO_MAIN()				throw compile_exception("No main function found!")

#endif
