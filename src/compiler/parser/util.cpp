#include "parser.hpp"

using namespace Tungsten;

const char* keywords[] = {
	"void",
	"int8",
	"uint8",
	"int16",
	"uint16",
	"int32",
	"uint32",
	"int64",
	"uint64",
	"float",
	"double",
	"bool",
	"false",
	"true",
	"null",
	"const",
	"constexpr",
	"namespace",
	"private",
	"protected",
	"public",
	"inline",
	"return",
	"if",
	"else",
	"switch",
	"case",
	"default",
	"while",
	"for",
	"do",
	"continue",
	"break",
	"try",
	"catch",
	"throw",
	"struct",
	"this",
	"super",
	"operator",
	"new",
	"delete",
	"template",
	"sizeof",
	"asm"
};

const char* operators[] = {
	"+",
	"-",
	"*",
	"/",
	"%",
	"**",
	"++",
	"--",
	"~",
	"&",
	"|",
	"^",
	"<<",
	">>",
	"=",
	"+=",
	"-=",
	"*=",
	"/=",
	"%=",
	"**=",
	"==",
	"!=",
	"<",
	"<=",
	">",
	">=",
	"?",
	":",
	"!",
	"(",
	"[",
	"."
};

const char* statement_types[] = {
	"root",
	"variable",
	"expression",
	"value",
	"string",
	"operator",
	"address-of",
	"indirection",
	"variable load",
	"variable increment",
	"variable decrement",
	"function call",
	"array load",
	"member access",
	"function",
	"namespace",
	"return",
	"if",
	"else",
	"switch",
	"case",
	"default",
	"while",
	"for",
	"do",
	"continue",
	"break",
	"try",
	"catch",
	"throw",
	"struct",
	"this",
	"super",
	"new",
	"free",
	"template",
	"sizeof",
	"asm"
};

bool Tungsten::is_space(const char c)
{
	return (c && (c <= 32 || c == 127));
}

bool Tungsten::is_letter(const char c)
{
	if(c >= 'a' && c <= 'z') return true;
	if(c >= 'A' && c <= 'Z') return true;

	return false;
}

bool Tungsten::is_numeric(const char c)
{
	return (c >= '0' && c <= '9');
}

bool Tungsten::is_valid_name_char(const char c)
{
	if(is_letter(c)) return true;
	if(is_numeric(c)) return true;
	if(c == '_') return true;

	return false;
}

bool Tungsten::is_access_specifier(const Keyword keyword)
{
	return (keyword == PRIVATE_K
		|| keyword == PROTECTED_K
		|| keyword == PUBLIC_K);
}

bool Tungsten::is_data_type(const Keyword keyword)
{
	return (keyword <= BOOL_K);
}

bool Tungsten::str_starts_with(const char* haystack, const char* needle)
{
	while(*haystack && *needle && *needle == *haystack) {
		++haystack;
		++needle;
	}

	return (*needle == '\0');
}

void Tungsten::skip_spaces(const char*& src)
{
	while(is_space(*src) || *src == '/') {
		while(is_space(*src)) ++src;

		if(str_starts_with(src, INLINE_COMMENT)) {
			src += strlen(INLINE_COMMENT);

			while(*src) {
				if(*src == '\n') {
					++src;
					break;
				}

				++src;
			}
		} else if(str_starts_with(src, COMMENT_OPEN)) {
			src += strlen(COMMENT_OPEN);

			while(*src) {
				if(str_starts_with(src, COMMENT_CLOSE)) {
					src += strlen(COMMENT_CLOSE);
					break;
				}

				++src;
			}
		}

		while(is_space(*src)) ++src;
	}
}

string Tungsten::get_word(const char*& src)
{
	skip_spaces(src);
	string word;

	while(*src && !is_space(*src)) {
		word += *src;
		++src;
	}

	return word;
}

string Tungsten::get_name(const char*& src)
{
	skip_spaces(src);
	string name;

	while(is_valid_name_char(*src)) {
		name += *src;
		++src;
	}

	return name;
}

string Tungsten::peek_name(const char* src)
{
	skip_spaces(src);
	string name;

	while(is_valid_name_char(*src)) {
		name += *src;
		++src;
	}

	return name;
}

Type Tungsten::get_type(const char*& src)
{
	const auto keyword = get_keyword(src);
	const auto constant = (keyword == CONST_K);

	const auto type = (constant ? get_keyword(src) : keyword);
	if(!is_data_type(type)) ERR_TYPE(type);

	size_t pointer = 0;

	while(*src == '*') {
		++pointer;
		++src;
	}

	const auto reference = (*src == REFERENCE);
	if(reference) ++src;

	// TODO Template handling?
	return Type(constant, type, pointer, reference);
}

bool Tungsten::check_keyword(const char* src)
{
	for(const auto& k : keywords) {
		if(str_starts_with(src, k)) return true;
	}

	return false;
}

const char* Tungsten::get_keyword(const Keyword keyword)
{
	return keywords[keyword];
}

Keyword Tungsten::get_keyword(const char*& src)
{
	const auto word = get_name(src);

	for(uint8_t k = 0; k < sizeof(keywords) / sizeof(const char*); ++k) {
		if(word == keywords[k]) return (Keyword) k;
	}

	ERR_UNKNOWN_KEYWORD(word);
}

Keyword Tungsten::peek_keyword(const char* src)
{
	const auto word = peek_name(src);

	for(uint8_t k = 0; k < sizeof(keywords) / sizeof(const char*); ++k) {
		if(word == keywords[k]) return (Keyword) k;
	}

	ERR_UNKNOWN_KEYWORD(word);
}

Keyword Tungsten::get_default_access(const StatementType type)
{
	switch(type) {
		case ROOT_S: {
			return PUBLIC_K;
		}

		case NAMESPACE_S: {
			return PROTECTED_K;
		}

		default:
		case STRUCT_S: {
			return PRIVATE_K;
		}
	}
}

bool Tungsten::check_operator(const char* src)
{
	skip_spaces(src);

	for(const auto& o : operators) {
		if(str_starts_with(src, o)) return true;
	}

	return false;
}

const char* Tungsten::get_operator(const Operator op)
{
	return operators[op];
}

Operator Tungsten::get_operator(const char*& src)
{
	skip_spaces(src);

	const auto size = sizeof(operators) / sizeof(const char*);

	size_t best = 0;
	size_t len = 0;

	for(size_t i = 0; i < size; ++i) {
		const auto& op = operators[i];

		if(str_starts_with(src, op)) {
			const auto l = strlen(op);

			if(l > len) {
				best = i;
				len = l;
			}
		}
	}

	if(len == 0) ERR_UNKNOWN_OPERATOR();

	src += len;
	return (Operator) best;
}

Operator Tungsten::peek_operator(const char* src)
{
	skip_spaces(src);

	const auto size = sizeof(operators) / sizeof(const char*);

	size_t best = 0;
	size_t len = 0;

	for(size_t i = 0; i < size; ++i) {
		const auto& op = operators[i];

		if(str_starts_with(src, op)) {
			const auto l = strlen(op);

			if(l > len) {
				best = i;
				len = l;
			}
		}
	}

	if(len == 0) ERR_UNKNOWN_OPERATOR();

	src += len;
	return (Operator) best;
}

void Tungsten::expect(const char*& src, const char c)
{
	if(*src == '\0') ERR_UEOF();
	if(*src != c) ERR_EXPECTED(c);

	++src;
}

char Tungsten::translate_escape_code(const char c)
{
	switch(c) {
		case '0': return '\0';
		case 'a': return '\a';
		case 'b': return '\b';
		case 't': return '\t';
		case 'n': return '\n';
		case 'v': return '\v';
		case 'f': return '\f';
		case 'r': return '\r';
		default: return c;
	}
}

int Tungsten::parse_int(const char*& src)
{
	// TODO Handle floats
	skip_spaces(src);

	const int8_t sign = (*src == '-' ? -1 : 1);
	if(*src == '+' || *src == '-') ++src;
	if(!is_numeric(*src)) ERR_INVALID_OPERAND();

	int n = 0;

	while(is_numeric(*src)) {
		n *= 10;
		n += *src - '0';
		++src;
	}

	return sign * n;
}

char Tungsten::parse_char(const char*& src)
{
	skip_spaces(src);

	expect(src, CHAR_DELIMITER);

	const bool backslash = (*src == BACKSLASH);
	if(backslash) ++src;

	const auto c = (backslash ? translate_escape_code(*src) : *src);
	++src;

	expect(src, CHAR_DELIMITER);

	return c;
}

string Tungsten::parse_string(const char*& src)
{
	skip_spaces(src);

	expect(src, STRING_DELIMITER);

	string str;

	while(*src && *src != STRING_DELIMITER) {
		str += *src;

		if(*src == BACKSLASH) {
			++src;
			if(*src == '\0') break;

			str += *src;
		}

		++src;
	}

	expect(src, STRING_DELIMITER);

	return str;
}

const char* Tungsten::get_statement_type(const StatementType type)
{
	return statement_types[type];
}

ErrorPosition Tungsten::get_pos(const char* begin, const char* c)
{
	vector<ErrorPosition> positions;

	for(auto i = begin; i < c; ++i) {
		if(str_starts_with(i, PC_FILE)) {
			positions.emplace_back();
			i += strlen(PC_FILE);

			while(*i && *i != '\n') {
				positions.back().file += *i;
				++i;
			}

			if(*i == '\0') {
				positions.back().file = "\033[31;1munknown\033[0m";
				return positions.back();
			}

			--positions.back().row;
		} else if(str_starts_with(i, PC_END)) {
			if(positions.empty()) {
				// TODO Error
			}

			positions.pop_back();

			i += strlen(PC_END);
			++i;
		}

		if(positions.empty()) {
			// TODO Error
		}

		auto& pos = positions.back();

		if(*i == '\n') {
			++pos.row;
			pos.column = 1;
		} else {
			++pos.column;
		}
	}

	if(positions.empty()) {
		// TODO Error
	}

	++positions.back().column;

	return positions.back();
}
