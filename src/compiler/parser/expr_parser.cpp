#include "parser.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

template<typename T>
static inline void add(AST* ast, const T value)
{
	ast->children_add<ASTValue>(value, sizeof(value));
}

template<>
inline void add(AST* ast, const string value)
{
	ast->children_add<ASTString>(value);
}

template<>
inline void add(AST* ast, const Operator value)
{
	ast->children_add<ASTOperator>(value);
}

static void parse_parameters(AST* ast, const char*& src)
{
	while(true) {
		skip_spaces(src);
		if(*src == BRACKET_CLOSE) break;
		if(*src == '\0') break;

		parse_child_expression(ast, src);

		if(*src == COMMA) {
			++src;
		} else {
			break;
		}
	}
}

// TODO Clean/rewrite
static void parse_access(AST* ast, const char*& src)
{
	const auto neg = (*src == '-');
	if(*src == '+' || *src == '-') ++src;

	while(true) {
		skip_spaces(src);

		string name;

		if(check_keyword(src)) {
			switch(peek_keyword(src)) {
				case THIS_K: {
					get_keyword(src);
					ast->children_add<ASTThis>();
					break;
				}

				case SUPER_K: {
					get_keyword(src);
					ast->children_add<ASTSuper>();
					break;
				}

				case SIZEOF_K: {
					get_keyword(src);
					parse_bracket(ast->scope_add<ASTSizeof>(), src,
						parse_child_expression);
					break;
				}

				default: {
					return;
				}
			}
		} else {
			name = get_name(src);
		}

		if(!check_operator(src)) {
			if(name.empty()) return;
			ast->children_add<ASTVarLoad>(name);

			return;
		}

		switch(peek_operator(src)) {
			case FUNCTION_CALL_O: {
				parse_bracket(ast->children_add<ASTFunctionCall>(name), src,
					parse_parameters);
				break;
			}

			case ARRAY_LOAD_O: {
				if(name.empty()) return;
				parse_array(ast->children_add<ASTArrayLoad>(name), src,
					parse_child_expression);
				break;
			}

			case MEMBER_LOAD_O: {
				if(name.empty()) return;
				ast->children_add<ASTVarLoad>(name);
				ast->children_add<ASTMemberLoad>(); // TODO Get member name
				++src;
				break;
			}

			default: {
				if(name.empty()) return;
				ast->children_add<ASTVarLoad>(name);
				break;
			}
		}
	}

	if(neg) {
		// TODO Use `neg` opcode
		// TODO Fix
		add(ast, -1);
		add(ast, OR_O);
	}
}

// TODO Clean/rewrite
static void parse_operand(AST* ast, const char*& src)
{
	skip_spaces(src);
	if(*src == '\0') ERR_UEOF();

	if(check_operator(src)) {
		switch(peek_operator(src)) {
			case AND_O: {
				get_operator(src);

				const auto name = get_name(src);
				if(name.empty()) ERR_NAME_EXPECTED();

				ast->children_add<ASTAddressOf>(name);
				return;
			}

			case MUL_O: {
				get_operator(src);

				parse_operand(ast, src);
				ast->children_add<ASTIndirection>();
				return;
			}

			case INCREMENT_O: {
				get_operator(src);

				const auto name = get_name(src);
				if(name.empty()) ERR_NAME_EXPECTED();

				ast->children_add<ASTVarIncrement>(name);
				return;
			}

			case DECREMENT_O: {
				get_operator(src);

				const auto name = get_name(src);
				if(name.empty()) ERR_NAME_EXPECTED();

				ast->children_add<ASTVarDecrement>(name);
				return;
			}

			case INVERSE_O: {
				get_operator(src);

				parse_operand(ast, src);
				add(ast, INVERSE_O);
				return;
			}

			default: {
				break;
			}
		}
	}

	switch(*src) {
		case BRACKET_OPEN: {
			++src;

			parse_child_expression(ast, src);

			skip_spaces(src);
			expect(src, BRACKET_CLOSE);
			return;
		}

		case CHAR_DELIMITER: {
			add(ast, parse_char(src));
			return;
		}

		case STRING_DELIMITER: {
			add(ast, parse_string(src));
			return;
		}
	}

	if(check_keyword(src)) {
		const auto keyword = get_keyword(src);

		switch(keyword) {
			case FALSE_K: {
				add<uint8_t>(ast, 0);
				return;
			}

			case TRUE_K: {
				add<uint8_t>(ast, 1);
				return;
			}

			case NULL_K: {
				add<uint64_t>(ast, 0);
				return;
			}

			case THIS_K:
			case SUPER_K:
			case SIZEOF_K: {
				parse_access(ast, src);
				return;
			}

			default: ERR_UNEXPECTED_KEYWORD(keyword);
		}
	}

	if(*src == '+' || *src == '-' || is_numeric(*src)) {
		add(ast, parse_int(src));
	} else if(*src == '+' || *src == '-' || is_valid_name_char(*src)) {
		parse_access(ast, src);
	} else {
		ERR_UNEXPECTED(*src);
	}
}

void parse_exponents(AST* ast, const char*& src)
{
	parse_operand(ast, src);

	while(true) {
		skip_spaces(src);
		if(*src == BRACKET_CLOSE || *src == COMMA) break;
		if(!check_operator(src)) break;

		const auto op = peek_operator(src);
		if(op != POW_O) break;
		src += strlen(get_operator(op));

		parse_operand(ast, src);
		add(ast, op);
	}
}

void parse_factors(AST* ast, const char*& src)
{
	parse_exponents(ast, src);

	while(true) {
		skip_spaces(src);
		if(*src == BRACKET_CLOSE || *src == COMMA) break;
		if(!check_operator(src)) break;

		const auto op = peek_operator(src);
		if(op != MUL_O && op != DIV_O && op != MOD_O) break;
		src += strlen(get_operator(op));

		parse_exponents(ast, src);
		add(ast, op);
	}
}

void parse_sums(AST* ast, const char*& src)
{
	parse_factors(ast, src);

	while(true) {
		skip_spaces(src);
		if(*src == BRACKET_CLOSE || *src == COMMA) break;
		if(!check_operator(src)) break;

		const auto op = peek_operator(src);
		if(op != ADD_O && op != SUB_O) break;
		src += strlen(get_operator(op));

		parse_factors(ast, src);
		add(ast, op);
	}
}

void parse_comparisons(AST* ast, const char*& src)
{
	parse_sums(ast, src);

	while(true) {
		skip_spaces(src);
		if(*src == BRACKET_CLOSE || *src == COMMA) break;
		if(!check_operator(src)) break;

		const auto op = peek_operator(src);
		if(op < EQUALS_O || op > HIGHER_OR_EQUALS_O) break;
		src += strlen(get_operator(op));

		parse_sums(ast, src);
		add(ast, op);
	}
}

void parse_bitwises(AST* ast, const char*& src)
{
	parse_comparisons(ast, src);

	while(true) {
		skip_spaces(src);
		if(*src == BRACKET_CLOSE || *src == COMMA) break;
		if(!check_operator(src)) break;

		const auto op = peek_operator(src);
		if(op < NOT_O || op > RIGHT_SHIFT_O) break;
		src += strlen(get_operator(op));

		parse_comparisons(ast, src);
		add(ast, op);
	}
}

void parse_assigns(AST* ast, const char*& src)
{
	parse_bitwises(ast, src);

	while(true) {
		skip_spaces(src);
		if(*src == BRACKET_CLOSE || *src == COMMA) break;
		if(!check_operator(src)) break;

		const auto op = peek_operator(src);
		if(op < ASSIGN_O || op > POW_ASSIGN_O) break;
		src += strlen(get_operator(op));

		parse_bitwises(ast, src);
		add(ast, op);
	}
}

void Tungsten::parse_child_expression(AST* ast, const char*& src)
{
	parse_assigns(ast->children_add<ASTExpression>(), src);
}

void Tungsten::parse_scope_expression(AST* ast, const char*& src)
{
	parse_assigns(ast->scope_add<ASTExpression>(), src);
}
