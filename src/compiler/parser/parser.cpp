#include "parser.hpp"

using namespace Tungsten;

void Tungsten::get_keywords(const StatementType type, const char*& src,
	Keyword& access_spec, Keyword& keyword, Type& data_type)
{
	keyword = peek_keyword(src);

	if(is_access_specifier(keyword)) {
		get_word(src);

		access_spec = keyword;
		keyword = peek_keyword(src);
	} else {
		access_spec = get_default_access(type);
	}

	if(keyword == CONST_K || is_data_type(keyword)) {
		keyword = VOID_K;
		data_type = get_type(src);
	} else {
		get_word(src);

		data_type = Type();
	}
}

static void parse_keyword(AST* ast, const char*& src)
{
	const auto keyword = peek_keyword(src);

	if(keyword == CONST_K || is_data_type(keyword)) {
		parse_variable(ast, src, false);
		return;
	}

	switch(get_keyword(src)) {
		case RETURN_K: {
			parse_bracket(ast->scope_add<ASTReturn>(), src,
				parse_child_expression);
			break;
		}

		case IF_K: {
			parse_if(ast->scope_add<ASTIf>(), src);
			break;
		}

		case SWITCH_K: {
			const auto s = ast->scope_add<ASTSwitch>();
			parse_bracket(s, src, parse_child_expression);
			parse_scope(s, src, parse_switch);

			break;
		}

		case WHILE_K: {
			parse_while(ast->scope_add<ASTWhile>(), src);
			break;
		}

		case FOR_K: {
			parse_for(ast->scope_add<ASTFor>(), src);
			break;
		}

		case DO_K: {
			parse_do_while(ast->scope_add<ASTDo>(), src);
			break;
		}

		case CONTINUE_K: {
			ast->scope_add<ASTContinue>();
			break;
		}

		case BREAK_K: {
			ast->scope_add<ASTBreak>();
			break;
		}

		case TRY_K: {
			// TODO
			break;
		}

		case THROW_K: {
			// TODO
			break;
		}

		case THIS_K: {
			// TODO
			break;
		}

		case SUPER_K: {
			// TODO
			break;
		}

		case OPERATOR_K: {
			// TODO
			break;
		}

		case NEW_K: {
			// TODO
			break;
		}

		case DELETE_K: {
			// TODO
			break;
		}

		case SIZEOF_K: {
			parse_bracket(ast->scope_add<ASTSizeof>(), src,
				parse_child_expression);
			break;
		}

		case ASM_K: {
			parse_scope(ast, src, parse_asm);
			break;
		}

		default: ERR_UNEXPECTED_KEYWORD(keyword);
	}
}

void Tungsten::parse_instructions(AST* ast, const char*& src)
{
	while(true) {
		skip_spaces(src);
		if(*src == SCOPE_CLOSE) break;
		if(*src == '\0') break;

		if(check_keyword(src)) {
			parse_keyword(ast, src);
		} else {
			parse_scope_expression(ast, src);
		}
	}
}

static void parse_declarations(AST* ast, const char*& src)
{
	while(true) {
		skip_spaces(src);
		if(*src == '\0') break;
		if(*src == SCOPE_CLOSE) break;

		// TODO Handle `inline`
		Keyword access_spec;
		Keyword keyword;
		Type type;
		get_keywords(ast->get_type(), src, access_spec, keyword, type);

		const auto name = get_name(src);

		if(keyword == NAMESPACE_K) {
			const auto s = ast->scope_add<ASTNamespace>(access_spec, name);
			parse_scope(s, src, parse_declarations);
		} else if(keyword == STRUCT_K) {
			// TODO Struct
		} else if(keyword == VOID_K) {
			skip_spaces(src);

			if(*src == BRACKET_OPEN) {
				const auto s = ast->scope_add<ASTFunction>(Function(access_spec,
					false, type, name));
				// TODO Handle constant function
				parse_bracket(s, src, parse_function_args);
				parse_scope(s, src, parse_instructions);
			} else {
				ERR_GLOBAL_VARIABLE();
			}
		} else {
			ERR_UNEXPECTED_KEYWORD(keyword);
		}
	}
}

shared_ptr<AST> Tungsten::parse(const string& source)
{
	shared_ptr<AST> ast(new ASTRoot);
	const auto begin = source.c_str();
	auto src = begin;

	try {
		parse_declarations(ast.get(), src);
		if(*src) ERR_UNEXPECTED(*src);
	} catch(const parse_exception& e) {
		const auto pos = get_pos(begin, src);
		throw parse_exception("\033[1;31merror\033[0m: "s + pos.file
			+ " |" + to_string(pos.row) + ":" + to_string(pos.column) + "|: "
			+ e.what());
	}

	return ast;
}
