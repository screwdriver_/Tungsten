#ifndef PARSER_HPP
# define PARSER_HPP

# include "../compiler.hpp"
# include "../ast/ast.hpp"

namespace Tungsten
{
	using namespace std;

	enum Punctuation : char
	{
		BRACKET_OPEN = '(',
		BRACKET_CLOSE = ')',

		ARRAY_OPEN = '[',
		ARRAY_CLOSE = ']',

		SCOPE_OPEN = '{',
		SCOPE_CLOSE = '}',

		TEMPLATE_OPEN = '<',
		TEMPLATE_CLOSE = '>',

		POINTER = '*',
		REFERENCE = '&',
		COMMA = ','
	};

	constexpr char CHAR_DELIMITER = '\'',
		STRING_DELIMITER = '\"',
		BACKSLASH = '\\';

	constexpr const char* INLINE_COMMENT = "//";
	constexpr const char* COMMENT_OPEN = "/*";
	constexpr const char* COMMENT_CLOSE = "*/";

	struct ErrorPosition
	{
		string file;
		size_t row, column;

		inline ErrorPosition()
		{
			reset();
		}

		inline void reset()
		{
			file.clear();
			row = 1;
			column = 1;
		}
	};

	class parse_exception : public exception
	{
		public:
			inline parse_exception(const string message)
				: message{message}
			{}

			inline const string& get_message() const
			{
				return message;
			}

			inline virtual const char* what() const noexcept
			{
				return message.c_str();
			}

		private:
			const string message;
	};

	bool is_space(const char c);
	bool is_letter(const char c);
	bool is_numeric(const char c);
	bool is_valid_name_char(const char c);

	bool is_access_specifier(const Keyword keyword);
	bool is_data_type(const Keyword keyword);

	bool str_starts_with(const char* haystack, const char* needle);

	void skip_spaces(const char*& src);
	string get_word(const char*& src);
	string get_name(const char*& src);
	string peek_name(const char* src);
	Type get_type(const char*& src);

	bool check_keyword(const char* src);
	const char* get_keyword(const Keyword keyword);
	Keyword get_keyword(const char*& src);
	Keyword peek_keyword(const char* src);

	bool check_operator(const char* src);
	const char* get_operator(const Operator op);
	Operator get_operator(const char*& src);
	Operator peek_operator(const char* src);

	void expect(const char*& src, const char c);

	char translate_escape_code(const char c);
	int parse_int(const char*& src);
	char parse_char(const char*& src);
	string parse_string(const char*& src);

	const char* get_statement_type(const StatementType type);
	ErrorPosition get_pos(const char* begin, const char* c);

	// -------------------------------------------------------------------------

	template<typename T>
	void parse_scope(AST* ast, const char*& src,
		void (*handle)(T* ast, const char*& src))
	{
		skip_spaces(src);
		expect(src, SCOPE_OPEN);
		skip_spaces(src);

		handle((T*) ast, src);

		skip_spaces(src);
		expect(src, SCOPE_CLOSE);
		skip_spaces(src);
	}

	template<typename T>
	void parse_bracket(AST* ast, const char*& src,
		void (*handle)(T* ast, const char*& src))
	{
		skip_spaces(src);
		expect(src, BRACKET_OPEN);
		skip_spaces(src);

		handle((T*) ast, src);

		skip_spaces(src);
		expect(src, BRACKET_CLOSE);
		skip_spaces(src);
	}

	template<typename T>
	void parse_array(AST* ast, const char*& src,
		void (*handle)(T* ast, const char*& src))
	{
		skip_spaces(src);
		expect(src, ARRAY_OPEN);
		skip_spaces(src);

		handle((T*) ast, src);

		skip_spaces(src);
		expect(src, ARRAY_CLOSE);
		skip_spaces(src);
	}

	void get_keywords(const StatementType type, const char*& src,
		Keyword& access_spec, Keyword& keyword, Type& data_type);

	void parse_instructions(AST* ast, const char*& src);

	void parse_variable(AST* ast, const char*& src, const bool argument);
	void parse_variable_definition(ASTVariable* ast,
		const char*& src, const bool arg);
	void parse_function_args(ASTFunction* ast, const char*& src);

	void parse_if(AST* ast, const char*& src);
	void parse_switch(AST* ast, const char*& src);
	void parse_while(AST* ast, const char*& src);
	void parse_do_while(AST* ast, const char*& src);
	void parse_for(AST* ast, const char*& src);
	void parse_asm(AST* ast, const char*& src);

	shared_ptr<AST> parse(const string& source);

	void parse_child_expression(AST* ast, const char*& src);
	void parse_scope_expression(AST* ast, const char*& src);
}

#endif
