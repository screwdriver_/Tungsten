#include "compiler.hpp"

using namespace Tungsten;

string Tungsten::read_file(const string& file)
{
	try {
		ifstream stream(file);

		stream.seekg(0, ios::end);
		const auto size = stream.tellg();
		stream.seekg(0);

		string buffer;
		buffer.resize(size);
		stream.read(&buffer[0], size);

		return buffer;
	} catch(const exception& e) {
		ERR_CANNOT_READ_FILE(file);
	}
}

static string precompile_data(const string& file, PrecompileData& data);

static string get_folder(const string& path)
{
	const auto pos = path.find_last_of('/');
	if(pos == string::npos) return "./";

	return string(path.cbegin(), path.cbegin() + pos);
}

static void define(PrecompileData& data,
	const string& name, const string& value)
{
	// TODO Check duplicate
	data.macros.emplace(name, value);
}

static void define(PrecompileData& data, const char*& line)
{
	const auto name = get_name(line);
	if(name.empty()) ERR_INVALID_PREPROCESSOR();

	define(data, name, get_word(line)); // TODO Get until `\n`
}

static void include(PrecompileData& data, const char*& line)
{
	const auto include = parse_string(line);
	if(include.empty()) ERR_INVALID_PREPROCESSOR();

	string path(data.current_folder);
	if(path.back() != '/') path += '/';
	path += include;

	// TODO Do not include if already included
	PrecompileData d(get_folder(path));
	data.output += precompile_data(path, d);

	for(const auto& m : d.macros) define(data, m.first, m.second);
}

static void eval_preprocessor(PrecompileData& data, const char* line)
{
	const auto keyword = get_word(line);

	if(keyword == "include") {
		include(data, line);
	} else if(keyword == "define") {
		define(data, line);
	} else {
		ERR_INVALID_PREPROCESSOR();
	}

	data.output += '\n';
}

static void replace_macros(PrecompileData& data, const char* line)
{
	while(*line) {
		if(!data.backslash && *line == '"') {
			data.in_string = !data.in_string;
		} else if(*line == '\\') {
			data.backslash = !data.backslash;
		}

		if(!data.in_string && *line == '$') {
			++line;

			const auto name = get_name(line);
			if(name.empty()) ERR_INVALID_PREPROCESSOR();

			const auto m = data.macros.find(name);
			if(m == data.macros.cend()) ERR_MACRO_NOT_FOUND(name);

			data.output += m->second;
		} else {
			data.output += *line;
			++line;
		}
	}
}

static string precompile_data(const string& file, PrecompileData& data)
{
	data.output += PC_FILE + file + '\n';

	stringstream ss(read_file(file));
	string buffer;

	while(getline(ss, buffer)) {
		buffer += '\n';
		auto str = buffer.c_str();

		skip_spaces(str);

		if(*str == '\0') {
			data.output += '\n';
			continue;
		}

		if(*str == '@') {
			++str;
			eval_preprocessor(data, str);
		} else {
			replace_macros(data, str);
		}
	}

	data.output += PC_END;
	return data.output;
}

string Tungsten::precompile(const string file)
{
	PrecompileData data(get_folder(file));
	return precompile_data(file, data);
}

string Tungsten::compile(const AST* ast)
{
	CompileData data;
	ast->compile(data);
	if(!data.has_main()) ERR_NO_MAIN();

	return data.dump();
}
